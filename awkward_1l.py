#!/usr/bin/env python3

import uproot
import awkward1 as ak
import numpy as np


def has_overlap(obj1, obj2, filter_dr):
    "Return mask array where obj1 has overlap with obj2 based on a filter function on deltaR (and pt of the first one)"
    obj1x, obj2x = ak.unzip(
        ak.cartesian([obj1[["pt", "eta", "phi"]], obj2[["eta", "phi"]]], nested=True)
    )
    dr = np.sqrt((obj1x.eta - obj2x.eta) ** 2 + (obj1x.phi - obj2x.phi) ** 2)
    return ak.any(filter_dr(dr, obj1x.pt), axis=2)


def match_dr(dr, pt, cone_size=0.2):
    return dr < cone_size


def match_boosted_dr(dr, pt, max_cone_size=0.4):
    return dr < np.minimum(*ak.broadcast_arrays(10000. / pt + 0.04, 0.4))


def get_evts(xaod_path, entrystop=None, xrootdsource={'chunkbytes': 16 * 1024}):

    electron_vars = [
        "pt",
        "eta",
        "phi",
        "DFCommonElectronsLHLooseBL",
        "DFCommonElectronsLHTight",
        "topoetcone20",
        "ptvarcone20_TightTTVA_pt1000",
    ]

    muon_vars = [
        "pt",
        "eta",
        "phi",
        "DFCommonGoodMuon",
        "topoetcone20",
        "ptvarcone30",
    ]

    jet_vars = [
        "pt",
        "eta",
        "phi",
        "Jvt",
        # "NumTrkPt500" # ... currently vector<vector ...
    ]

    pvtx_vars = [
        "x",
    ]

    # Read events into awkward structure
    evt = None
    with uproot.open(xaod_path, xrootdsource=xrootdsource) as f:
        tree = f["CollectionTree"]
        for obj_name, obj_branch, obj_vars in zip(
            ["electrons", "muons", "jets", "pvtx"],
            ["AnalysisElectrons", "AnalysisMuons", "AnalysisJets", "PrimaryVertices"],
            [electron_vars, muon_vars, jet_vars, pvtx_vars],
        ):
            arrays = tree.arrays(
                branches=[f"{obj_branch}AuxDyn.{var}" for var in obj_vars],
                namedecode="utf-8",
                entrystop=entrystop,
            )
            # from_awkward0 only nescessary until uproot supports awkward1
            arrays = ak.zip(
                {
                    k.replace(f"{obj_branch}AuxDyn.", ""): ak.from_awkward0(v)
                    for k, v in arrays.items()
                }
            )
            if evt is None:
                evt = ak.zip({obj_name: arrays}, depth_limit=1)
            else:
                evt[obj_name] = arrays


    return evt


def get_obj_sel(evt):
    # TODO: sort jets (wait for PR to get merged?)

    # skip events without primary vertex (important for data)
    evt = evt[ak.num(evt.pvtx.x) > 0]

    # lepton selection
    evt["electrons", "baseline"] = (
        (evt.electrons.DFCommonElectronsLHLooseBL == 1)
        & (evt.electrons.pt > 7000)
        & (np.abs(evt.electrons.eta) < 2.47)
    )
    evt["electrons", "signal"] = (
        evt.electrons.baseline
        & (evt.electrons.DFCommonElectronsLHTight == 1)
        & (evt.electrons.topoetcone20 / evt.electrons.pt < 0.2)
        & (evt.electrons.ptvarcone20_TightTTVA_pt1000 / evt.electrons.pt < 0.15)
    )
    evt["muons", "baseline"] = (
        (evt.muons.DFCommonGoodMuon == 1)
        & (evt.muons.pt > 6000)
        & (np.abs(evt.muons.eta) < 2.7)
    )
    evt["muons", "signal"] = (
        evt.muons.baseline
        & (np.abs(evt.muons.eta) < 2.5)
        & (evt.muons.topoetcone20 / evt.muons.pt < 0.3)
        & (evt.muons.ptvarcone30 / evt.muons.pt < 0.15)
    )

    # jet selection
    evt["jets", "baseline"] = evt.jets.pt >= 20000
    jvt_pt_min = 20000
    jvt_pt_max = 60000
    jvt_eta_max = 2.4
    evt["jets", "passJvt"] = (
        ((evt.jets.pt >= jvt_pt_min) & (evt.jets.pt < jvt_pt_max) & (evt.jets.Jvt > 0.2) & (np.abs(evt.jets.eta) < jvt_eta_max))
        | ((evt.jets.pt >= jvt_pt_max) | (evt.jets.pt < jvt_pt_min) | (np.abs(evt.jets.eta) >= jvt_eta_max))
    )
    evt["jets", "signal"] = evt.jets.baseline & evt.jets.passJvt & (np.abs(evt.jets.eta) < 4.5)

    ## "simplified" overlap removal
    # remove jets overlapping with electrons
    evt["jets", "passOR"] = (
        evt.jets.baseline
        & (
            ~has_overlap(
                evt.jets,
                evt.electrons[evt.electrons.baseline],
                match_dr
            )
        )
    )
    # remove electrons overlapping (boosted cone) with remaining jets (if they pass jvt)
    evt["electrons", "passOR"] = (
        evt.electrons.baseline
        & (
            ~has_overlap(
                evt.electrons,
                evt.jets[evt.jets.passOR & evt.jets.passJvt],
                match_boosted_dr
            )
        )
    )
    # remove jets overlapping with muons (TODO: check ntrack requirement, ghost association)
    evt["jets", "passOR"] = evt.jets.passOR & (~has_overlap(evt.jets, evt.muons[evt.muons.baseline], match_dr))
    # remove muons overlapping (boosted cone) with remaining jets (if they pass jvt)
    evt["muons", "passOR"] = (
        evt.muons.baseline
        & (
            ~has_overlap(
                evt.muons,
                evt.jets[evt.jets.baseline & evt.jets.passOR & evt.jets.passJvt],
                match_boosted_dr
            )
        )
    )
    return evt


if __name__ == "__main__":

    evt = get_evts(
        "root://lcg-lrz-rootd.grid.lrz.de:1094/pnfs/lrz-muenchen.de/data/atlas/dq2/atlaslocalgroupdisk/rucio/user/elmsheus/e7/9e/user.elmsheus.21230205.EXT0._000002.DAOD_PHYSLITE.mc.pool.root",
        entrystop=100,
    )
    evt = get_obj_sel(evt)
